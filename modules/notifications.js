function Notifications() {
    let helper = new Helper();

    // Send a push notification when a mission is completed (or added)
    this.fleetNotifications = function() {
        window.attackAnnounced = false;
        window.missionCount = null;

        window.setInterval(function() {
            let eventBox = document.getElementById('eventboxFilled');
            // There are a few events that tell whether or not a mission is started or ended;

            // The mission count can increase; a new fleet is incoming.
            var currentMissionCount = 0;
            if (eventBox.style.display !== 'none') {
                let eventList = eventBox.getElementsByClassName('event_list')[0];
                currentMissionCount = parseInt(eventList.firstChild.textContent.split(/:/)[0].replace(/[^0-9]/g, ''), 10);
            }
            if (window.missionCount !== null && window.missionCount < currentMissionCount) {
                GM_notification('New fleet incoming', 'Mission started');
            }
            window.missionCount = currentMissionCount;

            // Mission timer can expire, then a mission is completed
            let tempcounter = helper.durationToSeconds(document.getElementById('tempcounter').innerText);
            if (tempcounter == 1) {
                GM_notification('A mission is completed', 'Mission completed');
            }

            // An incoming hostile fleet also deserves a notification
            let attackAlert = document.getElementById('attack_alert');
            if (attackAlert.className.indexOf('noAttack') < 0 && window.attackAnnounced === false) {
                window.attackAnnounced = true;
                GM_notification('Warning! New hostile fleet incoming!', 'Warning!!');
            }

        }, 1000);
    }

}
