function ResourcesView() {
    let helper = new Helper;

    this.createButton = function() {
        // On the resource center, we want a button to copy the resource amounts to clipboard (useful for Excel)
        var html = '<input class="btn_blue" type="button" value="To clipboard" id="copydata">';
        document.getElementById('factor').getElementsByClassName('factorbutton')[0].innerHTML += html;
        document.getElementById('copydata').onclick = function() {
            document.getElementById('copydata').className = ""; // It looks like it does something
            var ta = document.createElement('textarea');
            ta.value = helper.getMetal()+"\t"+helper.getCrystal()+"\t"+helper.getDeuterium();
            document.getElementById('factor').appendChild(ta);
            ta.focus();
            ta.select();
            try {
                document.execCommand('copy');
            } catch (ex) {
                alert('Can\'t write to clipboard');
            }
            document.getElementById('factor').removeChild(ta);
            window.setTimeout(function() {
                document.getElementById('copydata').className = "btn_blue";
            }, 500);
        }
    };
}
