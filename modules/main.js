function MainView() {
    let helper = new Helper;

    const planet = document.querySelector('.planetlink.active').querySelector('.planet-name').innerHTML;
    const metal = helper.getMetal();
    const crystal = helper.getCrystal();
    const deuterium = helper.getDeuterium();

    let self = this;

    document.getElementById('eventboxFilled').addEventListener('click', function() {
        window.setTimeout(function() {
            let eventDetails = document.getElementById('js_eventDetailsOpen');

            if (eventDetails.style.display === 'inline') {
                self.init(planet, metal, crystal, deuterium);
            } else {
                self.removeResourceBox();
            }
        },250);
    }, false);

    this.init = function(planet, internalMetal, internalCrystal, internalDeuterium) {
        // Only load if the eventContent is clicked, because it will receive data from Ajax
        let eventContainer = document.getElementById('eventContent');

        // Get all the fleet data currently in progress
        let events = eventContainer.querySelectorAll('tbody tr');

        // Go through each fleet and get the tooltip data
        for (let i = 0; i < events.length; i++) {
            let fleet = events[i].querySelector('[class$="icon_movement"]');
            let destination = events[i].querySelector('.destFleet').innerText;

            if (!fleet || planet !== destination) {
                continue;
            }

            let tooltip = this.convertToHTML(fleet.querySelector('.tooltip').getAttribute('title'));

            let fleetData = this.getFleetData(tooltip.querySelectorAll('.fleetinfo tbody tr'));

            internalMetal += fleetData.Metaal;
            internalCrystal += fleetData.Kristal;
            internalDeuterium += fleetData.Deuterium;
        }

        if (internalMetal !== metal || internalCrystal !== crystal || internalDeuterium !== deuterium) {
            this.addToResourceBox(internalMetal, internalCrystal, internalDeuterium);
        }
    };

    this.convertToHTML = function(string) {
        let html = new DOMParser().parseFromString(string, "text/html");

        return html.querySelector('.htmlTooltip');
    };

    this.getFleetData = function(rows) {
        let fleetData = [];

        for (let i = 0; i < rows.length; i++) {
            if (rows[i].children[1]) {
                let key = rows[i].children[0].innerHTML;
                let data = rows[i].children[1].innerHTML;

                fleetData[key.replace(/\:/g, '')] = parseInt(data.replace(/\./g, ''));
            }
        }

        return fleetData;
    };

    this.addToResourceBox = function(metal, crystal, deuterium) {
        let metalSpan = document.getElementById('metalSpan');
        let crystalSpan = document.getElementById('crystalSpan');
        let deuteriumSpan = document.getElementById('deuteriumSpan');

        let metalBox = document.getElementById('metal_box');
        let crystalBox = document.getElementById('crystal_box');
        let deuteriumBox = document.getElementById('deuterium_box');

        let span = document.createElement('span');
        span.classList.add('value');
        span.style.color = '#ffd700';

        if (metalSpan === null) {
            metalSpan = span.cloneNode(true);
            metalSpan.setAttribute('id', 'metalSpan');
            metalBox.appendChild(metalSpan);
        }
        metalSpan.innerHTML = '(' + (metal).toLocaleString('nl') + ')';

        if (crystalSpan === null) {
            crystalSpan = span.cloneNode(true);
            crystalSpan.setAttribute('id', 'crystalSpan');
            crystalBox.appendChild(crystalSpan);
        }
        crystalSpan.innerHTML = '(' + (crystal).toLocaleString('nl') + ')';

        if (deuteriumSpan === null) {
            deuteriumSpan = span.cloneNode(true);
            deuteriumSpan.setAttribute('id', 'deuteriumSpan');
            deuteriumBox.appendChild(deuteriumSpan);
        }
        deuteriumSpan.innerHTML = '(' + (deuterium).toLocaleString('nl') + ')';
    };

    this.removeResourceBox = function() {
        let metalSpan = document.getElementById('metalSpan');
        let crystalSpan = document.getElementById('crystalSpan');
        let deuteriumSpan = document.getElementById('deuteriumSpan');

        if (metalSpan !== null) {
            metalSpan.remove();
        }

        if (crystalSpan !== null) {
            crystalSpan.remove();
        }

        if (deuteriumSpan !== null) {
            deuteriumSpan.remove();
        }
    };
}
