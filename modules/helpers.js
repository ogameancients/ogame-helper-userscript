function Helper() {
    this.getResource = function(type) {
        return parseInt(document.getElementById('resources_'+type).innerHTML.replace(/\./g, ''), 10);
    };

    this.getMetal = function() {
        return this.getResource('metal');
    };

    this.getCrystal = function() {
        return this.getResource('crystal');
    };

    this.getDeuterium = function() {
        return this.getResource('deuterium');
    };

    /**
     * Converts a duration text to amount in seconds
     *
     * @param {string} duration
     *
     * @returns {number}
     */
    this.durationToSeconds = function(duration) {
        let returnValue = 0;
        let parts = duration.split(/[\s]{1,}/g);
        for (let iterator = 0; iterator < parts.length; ++iterator) {
            let digits = parts[iterator].replace(/[^0-9]/g, '');
            let letter = parts[iterator].replace(/[0-9]/g, '');
            if (letter == 's') {
                returnValue += digits;
            }
            if (letter == 'm') {
                returnValue += (digits * 60);
            }
            if (letter == 'h' || letter == 'u') {
                returnValue += (digits * 3600);
            }
            if (letter == 'd') {
                returnValue += (digits * 86400);
            }
            if (letter == 'w') {
                returnValue += (digits * 604800);
            }
        }
        return returnValue;
    }
}
