function FleetView() {
    let helper = new Helper;

    this.cargoShips = {
        small: {
            id: 202,
            capacity: config.ships.civil.small_cargo_ship.storage_capacity
        },
        large: {
            id: 203,
            capacity: config.ships.civil.large_cargo_ship.storage_capacity
        },
        recycler: {
            id: 209,
            capacity: config.ships.civil.recycler.storage_capacity
        }
    };

    // Clicking a cargo ship only adds the required amount of ships to send all resources from that planet
    this.cargoRequired = function(shipTypes) {
        if (!shipTypes instanceof Array) {
            shipTypes = [shipType];
        }

        for (let i = 0; i < shipTypes.length; i++) {
            let self = this;
            let shipType = shipTypes[i];
            let cargoButtonId = 'button' + this.cargoShips[shipType].id;
            this.cargoShips[shipType].button = document.getElementById(cargoButtonId).getElementsByTagName('a')[0];
            this.cargoShips[shipType].button.oldOnClick = this.cargoShips[shipType].button.onclick;
            this.cargoShips[shipType].button.onclick = function(e) {
                e.stopPropagation();
                var total = helper.getMetal()+helper.getCrystal()+helper.getDeuterium();
                var ships_required = Math.ceil(total/self.cargoShips[shipType].capacity);
                let shipElementId = 'ship_' + self.cargoShips[shipType].id;
                let shipElement = document.getElementById(shipElementId);
                if (shipElement.value == ships_required) {
                    self.cargoShips[shipType].button.oldOnClick();
                } else if (shipElement.value) {
                    shipElement.value = "";
                } else {
                    shipElement.value = ships_required;
                }
                checkShips("shipsChosen");
            }
        }
    };

    this.recallDestination = function() {
        let currentPlanet = document.head.querySelector("meta[name=ogame-planet-coordinates]").content;
        let selectedPlanet = document.getElementById('galaxy').value+":"+document.getElementById('system').value+":"+document.getElementById('position').value;
        if (currentPlanet == selectedPlanet || selectedPlanet == "::") { // When current planet or no planet is selected, select main planet instead
            document.getElementById('galaxy').value = GM_getValue('fleet_dest_galaxy');
            document.getElementById('system').value = GM_getValue('fleet_dest_system');
            document.getElementById('position').value = GM_getValue('fleet_dest_position');
            document.getElementById('type').value = GM_getValue('fleet_dest_type'); // 1=planet, 2=debris, 3=moon
            updateVariables();
        }
    };

    this.initDestinationRecall = function() {
        this.recallDestination();

        window.onbeforeunload = function() {
            GM_setValue('fleet_dest_galaxy', document.getElementById('galaxy').value);
            GM_setValue('fleet_dest_system', document.getElementById('system').value);
            GM_setValue('fleet_dest_position', document.getElementById('position').value);
            GM_setValue('fleet_dest_type', document.getElementById('type').value); // 1=planet, 2=debris, 3=moon
        };
    };

    this.fullCargo = function() {
        // Cargo ships involved and no mission selected? Select transport -> all
        if ((document.querySelector('input[name=am202]') !== null || document.querySelector('input[name=am203]') !== null || document.querySelector('input[name=am209]') !== null) && document.querySelector('input[name=mission]').value == "0") {
            document.getElementById('missionButton3').onclick();
            document.getElementById('allresources').onclick();
        }
    }
}
