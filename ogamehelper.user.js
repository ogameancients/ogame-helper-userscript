// ==UserScript==
// @name         OGame Helper
// @namespace    ogameancients/ogame-helper-userscript
// @homepageURL  https://bitbucket.org/ogameancients/
// @icon64URL    https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/assets/ogame-icon.jpg
// @updateURL    https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/ogamehelper.user.js
// @downloadURL  https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/ogamehelper.user.js
// @supportURL   https://bitbucket.org/ogameancients/ogame-helper-userscript/issues/new
// @version      0.6.0
// @description  try to take over the world! Or at least OGame
// @author       Stefan Thoolen & Tim van den Oever
// @match        https://*.ogame.gameforge.com/*
// @grant        GM_notification
// @grant        GM_setValue
// @grant        GM_getValue
//
// @require      https://code.jquery.com/jquery-3.4.0.min.js
// @require      https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/modules/main.js
// @require      https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/modules/helpers.js
// @require      https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/modules/resources.js
// @require      https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/modules/fleet.js
// @require      https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/modules/notifications.js
// ==/UserScript==

const config = {
    "ships": {
        "civil": {
            "small_cargo_ship": {
                "storage_capacity": 7000
            },
            "large_cargo_ship": {
                "storage_capacity": 35000
            },
            "recycler": {
                "storage_capacity": 29000
            }
        }
    }
};

(function() {
    'use strict';

    // Fleet View - step 1
    if (document.location.href.match('fleet1')) {
        let fleet = new FleetView;

        fleet.cargoRequired(['small', 'large', 'recycler']);
    }

    // Fleet View - step 2
    if (document.location.href.match('fleet2')) {
        let fleet = new FleetView;

        fleet.initDestinationRecall();
    }

    // Fleet View - step 3
    if (document.location.href.match('fleet3')) {
        let fleet = new FleetView;

        fleet.fullCargo();
    }

    // Resources View
    if (document.location.href.match('resourceSettings')) {
        let resources = new ResourcesView;

        resources.createButton();
    }

    // Notifications are initialized on all views
    let notifications = new Notifications;
    notifications.fleetNotifications();

    let main = new MainView;
})();
