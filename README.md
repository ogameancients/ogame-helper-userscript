# OGame userscript

Userscript to add some additional features to OGame.

## Installation instructions

1. Install Tampermonkey from https://tampermonkey.net/ (or any other userscript plugin)
2. Restart your webbrowser
3. Click [this button](https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/ogamehelper.user.js) or add [ogamehelper.user.js](https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/ogamehelper.user.js) as script manually.

## Additional features

* "To clipboard" button on the Resource Settings screen.
* When clicking on cargo ships at the fleet page, it will first select the required amount for all resources, instead of just all ships.
* By default, send fleets to the main planet instead of the current planet (which is impossible)
* When sending cargo ships, by default, select mission type "Transport" with all resources
* Desktop notifications when a mission is completed or a new fleet is incoming
* When folding open incoming fleets, predict how many resources you'll have when it's all arrived
